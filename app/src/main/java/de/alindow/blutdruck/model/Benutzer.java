package de.alindow.blutdruck.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "benutzer")
public class Benutzer {

  @PrimaryKey
  @NonNull
  @ColumnInfo(name = "email")
  private String email = "noreply";

  @ColumnInfo(name = "vorname")
  private String vorname;

  @ColumnInfo(name = "nachname")
  private String nachname;

  @Ignore
  public Benutzer(@NonNull String email) {
    this.email = email;
  }

  public Benutzer(@NonNull String email, String vorname, String nachname) {
    this.email = email;
    this.vorname = vorname;
    this.nachname = nachname;
  }

  public String getEmail() {
    return this.email;
  }

  public String getVorname() { return vorname; }

  public String getNachname() { return nachname; }

  public void setEmail(String email) { this.email = email; }

  public void setVorname(String vorname) { this.vorname = vorname; }

  public void setNachname(String nachname) { this.nachname = nachname; }
}
