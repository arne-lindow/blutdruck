package de.alindow.blutdruck.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "Messung")
public class Messung {

  @PrimaryKey
  @NonNull
  @ColumnInfo(name = "zeitpunkt")
  private Date zeitpunkt = new Date();

  @NonNull
  @ColumnInfo(name = "benutzer")
  private Benutzer benutzer;

  @ColumnInfo(name = "systolisch")
  private Integer systolisch;

  @ColumnInfo(name = "diastolisch")
  private Integer diastolisch;

  @ColumnInfo(name = "puls")
  private Integer puls;

  @ColumnInfo(name = "bemerkung")
  private String bemerkung;

  @NonNull
  public Date getZeitpunkt() {
    return zeitpunkt;
  }

  public void setZeitpunkt(@NonNull Date zeitpunkt) {
    this.zeitpunkt = zeitpunkt;
  }

  @NonNull
  public Benutzer getBenutzer() {
    return benutzer;
  }

  public void setBenutzer(@NonNull Benutzer benutzer) {
    this.benutzer = benutzer;
  }

  public Integer getSystolisch() {
    return systolisch;
  }

  public void setSystolisch(Integer systolisch) {
    this.systolisch = systolisch;
  }

  public Integer getDiastolisch() {
    return diastolisch;
  }

  public void setDiastolisch(Integer diastolisch) {
    this.diastolisch = diastolisch;
  }

  public Integer getPuls() {
    return puls;
  }

  public void setPuls(Integer puls) {
    this.puls = puls;
  }

  public String getBemerkung() {
    return bemerkung;
  }

  public void setBemerkung(String bemerkung) {
    this.bemerkung = bemerkung;
  }
}
