package de.alindow.blutdruck.view;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import de.alindow.blutdruck.data_access.BenutzerRepository;
import de.alindow.blutdruck.model.Benutzer;

public class BenutzerViewModel extends AndroidViewModel {

  private BenutzerRepository repository;
  private LiveData<List<Benutzer>> allBenutzer;

  public BenutzerViewModel (Application application) {
    super(application);
    repository = new BenutzerRepository(application);
    allBenutzer = repository.getAllBenutzer();
  }

  LiveData<List<Benutzer>> getAllBenutzer() { return allBenutzer; }

  public void insert(Benutzer benutzer) { repository.insert(benutzer); }


}
