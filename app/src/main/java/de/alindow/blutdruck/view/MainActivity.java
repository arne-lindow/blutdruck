package de.alindow.blutdruck.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import de.alindow.blutdruck.R;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }
}
