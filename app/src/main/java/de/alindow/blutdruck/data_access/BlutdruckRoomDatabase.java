package de.alindow.blutdruck.data_access;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import de.alindow.blutdruck.model.Benutzer;

@Database(entities = {Benutzer.class}, version = 1)
public abstract class BlutdruckRoomDatabase extends RoomDatabase {

  public abstract BenutzerDao benutzerDao();

  private static volatile BlutdruckRoomDatabase INSTANCE;

  static BlutdruckRoomDatabase getDatabase(final Context context) {
    if (INSTANCE == null) {
      synchronized (BlutdruckRoomDatabase.class) {
        if (INSTANCE == null) {
          INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
              BlutdruckRoomDatabase.class, "blutdruck_database")
              .build();
        }
      }
    }
    return INSTANCE;
  }

}
