package de.alindow.blutdruck.data_access;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import de.alindow.blutdruck.model.Benutzer;

@Dao
public interface BenutzerDao {

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  void insert(Benutzer benutzer);

  @Query("DELETE FROM benutzer")
  void deleteAll();

  @Query("SELECT * from benutzer ORDER BY email ASC")
  LiveData<List<Benutzer>> getAllBenutzer();
  // List<Benutzer> getAllBenutzer();
}
