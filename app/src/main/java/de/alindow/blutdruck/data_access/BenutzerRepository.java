package de.alindow.blutdruck.data_access;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import de.alindow.blutdruck.model.Benutzer;

public class BenutzerRepository {

  private BenutzerDao benutzerDao;
  private LiveData<List<Benutzer>> allBenutzer;

  public BenutzerRepository(Application application) {
    BlutdruckRoomDatabase db = BlutdruckRoomDatabase.getDatabase(application);
    benutzerDao = db.benutzerDao();
    allBenutzer = benutzerDao.getAllBenutzer();
  }

  public LiveData<List<Benutzer>> getAllBenutzer() {
    return allBenutzer;
  }


  public void insert (Benutzer benutzer) {
    new insertAsyncTask(benutzerDao).execute(benutzer);
  }

  private static class insertAsyncTask extends AsyncTask<Benutzer, Void, Void> {

    private BenutzerDao asyncTaskDao;

    insertAsyncTask(BenutzerDao dao) {
      asyncTaskDao = dao;
    }

    @Override
    protected Void doInBackground(final Benutzer... params) {
      asyncTaskDao.insert(params[0]);
      return null;
    }
  }
}
